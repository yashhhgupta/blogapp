export interface blog {
    id: number;
    title: string;
    description: string;
    content: string;
    author: string;
}

export interface user {
    username: string;
    password: string;
    role: "admin" | "user";
    totalBlogs?: number;
}