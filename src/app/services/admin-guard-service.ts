import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthGuardService } from './auth-guard.service';

@Injectable({
  providedIn: 'root',
})
export class AdminGuardService implements CanActivate {

    isAdmin: boolean = false;

  constructor(private authService: AuthGuardService, private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const currentUser = this.authService.user;
    if (currentUser) {
      if (currentUser.role === 'admin') {
          console.log('Admin access granted');
          this.isAdmin = true;
        return true;
      } else {
        console.log('Non-admin user redirected to home');
        this.router.navigate(['/blog']);
        return false;
      }
    }
    console.log('User not logged in, redirecting to login');
    this.router.navigate(['/login']);
    return false;
  }
}
