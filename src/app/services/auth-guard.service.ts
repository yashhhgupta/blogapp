import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from '@angular/router';
import { user } from '../types';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  isLoggedIn: boolean = false;
  user: user | null = null;

  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if (this.isLoggedIn) {
      return true;
    } else {
      alert('Please log in');
      this.router.navigate(['/login']);
      return false;
    }
  }
}
