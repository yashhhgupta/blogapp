import { Injectable } from '@angular/core';
import { blog } from '../types';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  user: string | null = null;
  constructor() {
  }

  createBlog(blog: blog) {
    let blogs = this.getBlogs();
    blogs.push(blog);
    localStorage.setItem('blogs', JSON.stringify(blogs));
  }

  getBlogs() {
    let blogs = localStorage.getItem('blogs');
    return blogs ? JSON.parse(blogs) : [];
  }

  getBlog(blogId: number | string | null | undefined) {
    if (!blogId) {
      return null;
    }
    let blogs = this.getBlogs();
    let temp = blogs.find((blog: blog) => blog.id == blogId);
    return temp;
  }

  updateBlog(blogId: number, updatedBlog: blog) {
    let blogs = this.getBlogs();
    let index = blogs.findIndex((blog: any) => blog.id === blogId);
    if (index !== -1) {
      blogs[index] = { ...blogs[index], ...updatedBlog };
      localStorage.setItem('blogs', JSON.stringify(blogs));
    }
  }

  deleteBlog(blogId: number) {
    let blogs = this.getBlogs();
    let filteredBlogs = blogs.filter((blog: any) => blog.id !== blogId);
    localStorage.setItem('blogs', JSON.stringify(filteredBlogs));
  }

  getMyBlogs(username: string | null | undefined) {
    if (!username) {
      return [];
    }
    let blogs = this.getBlogs();
    return blogs.filter((blog: any) => blog.author === username);
  }

  addInitialBlogs() { 
    let blogs = [
      {
        id: 1,
        title: 'The Art of Mindfulness',
        description: 'Exploring the practice of mindfulness and its benefits',
        content:
          'In a fast-paced world, mindfulness offers a path to peace and clarity. Learn about the origins of mindfulness, practical techniques to incorporate it into your daily life, and the science-backed benefits it brings to mental and emotional well-being.',
        author: 'user',
      },
      {
        id: 2,
        title: 'The Rise of Artificial Intelligence',
        description:
          "Understanding AI's impact on society and the future of work",
        content:
          'Artificial Intelligence has rapidly evolved, shaping various aspects of our lives from healthcare to transportation. Dive into the history of AI, its current applications, ethical considerations, and what the future holds for AI-driven technologies and job landscapes.',
        author: 'john_doe',
      },
      {
        id: 3,
        title: 'Exploring Sustainable Living',
        description: 'Tips and tricks for embracing an eco-friendly lifestyle',
        content:
          "As environmental concerns grow, many are adopting sustainable practices. Discover actionable steps to reduce your carbon footprint, embrace renewable energy sources, practice mindful consumption, and contribute positively to the planet's health.",
        author: 'user',
      },
      {
        id: 4,
        title: 'The Power of Storytelling in Marketing',
        description:
          'Harnessing the narrative to connect with audiences and drive engagement',
        content:
          'Storytelling has become a cornerstone of effective marketing strategies. Uncover the psychology behind storytelling, examples of successful brand narratives, and how businesses can craft compelling stories to captivate customers, build brand loyalty, and drive conversions.',
        author: 'user',
      },
    ];
    let users = [
      {
        username: 'john_doe',
        password: 'password',
        role: 'admin',
      },
      {
        username: 'user',
        password: 'password',
        role: 'user',
      },
      {
        username: 'admin',
        password: 'admin',
        role: 'admin',
      },
    ];
    localStorage.setItem('blogs', JSON.stringify(blogs));
    localStorage.setItem('users', JSON.stringify(users));
  }

}
