import { Injectable } from '@angular/core';
import { user } from '../types';
import { BlogService } from './blog.service';
import { blog } from '../types';


@Injectable({
  providedIn: 'root',
})
export class UserService {
  blogs : blog[] = [];

  constructor(
    private blogService: BlogService,

  ) {
    this.blogs = this.blogService.getBlogs();
  }

  getUsers() {
    let users = localStorage.getItem('users');
    //add total blogs to each user
    if(users){
      let usersArray = JSON.parse(users);
      usersArray.forEach((user: user) => {
        user.totalBlogs = this.blogs.filter((blog: blog) => blog.author === user.username).length;
      });
      return usersArray;
    }

    return [];
  }

  addUser(user: user) {
    let users = this.getUsers();
    users.push(user);
    localStorage.setItem('users', JSON.stringify(users));
  }

  updateUser(username: string, updatedUser: user) {
    let users = this.getUsers();
    let index = users.findIndex((user: any) => user.username === username);
    if (index !== -1) {
      users[index] = { ...users[index], ...updatedUser };
      localStorage.setItem('users', JSON.stringify(users));
    }
  }

  deleteUser(username: string) {
    let users = this.getUsers();
    let filteredUsers = users.filter((user: any) => user.username !== username);
    localStorage.setItem('users', JSON.stringify(filteredUsers));
    //delete all blogs of the user
    let temp= this.blogs.filter((blog: blog) => blog.author == username);
    temp.forEach((blog: blog) => {
      this.blogService.deleteBlog(blog.id);
    });
  }

  getUser(username: string) {
    let users = this.getUsers();
    return users.find((user: any) => user.username === username);
  }
}
