import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/home/sidebar/sidebar.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CustomButtonComponent } from './components/common/custom-button/custom-button.component';
import { FormsModule } from '@angular/forms';
import { CardComponent } from './components/common/card/card.component';
import { CreateBlogComponent } from './components/home/create-blog/create-blog.component';
import { BlogPageComponent } from './components/home/blog-page/blog-page.component';
import { ManageUsersComponent } from './components/home/manage-users/manage-users.component';
import { MyBlogPageComponent } from './components/home/my-blog-page/my-blog-page.component';
import { ViewBlogComponent } from './components/home/view-blog/view-blog.component';
import { ErrorComponent } from './components/home/error/error.component';
import { TooltipDirective } from './directives/tooltip.directive';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    LoginComponent,
    HomeComponent,
    CustomButtonComponent,
    CardComponent,
    CreateBlogComponent,
    BlogPageComponent,
    ManageUsersComponent,
    MyBlogPageComponent,
    ViewBlogComponent,
    ErrorComponent,
    TooltipDirective,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
