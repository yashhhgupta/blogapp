import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { CreateBlogComponent } from './components/home/create-blog/create-blog.component';
import { AuthGuardService } from './services/auth-guard.service';
import { BlogPageComponent } from './components/home/blog-page/blog-page.component';
import { ManageUsersComponent } from './components/home/manage-users/manage-users.component';
import { MyBlogPageComponent } from './components/home/my-blog-page/my-blog-page.component';
import { ViewBlogComponent } from './components/home/view-blog/view-blog.component';
import { AdminGuardService } from './services/admin-guard-service';
import { ErrorComponent } from './components/home/error/error.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full',
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuardService],
    children: [
      { path: 'blog', component: BlogPageComponent },
      { path: 'myBlog', component: MyBlogPageComponent },
      { path: 'createBlog', component: CreateBlogComponent },
      { path: 'editBlog/:id', component: CreateBlogComponent},
      {
        path: 'blog/:id',
        component: ViewBlogComponent,
      },
      {
        path: 'manageUsers',
        component: ManageUsersComponent,
        canActivate: [AdminGuardService],
      },
    ],
  },
  
    { path: '**', component: ErrorComponent }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
