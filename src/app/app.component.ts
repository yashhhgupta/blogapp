import { Component } from '@angular/core';
import { AuthGuardService } from './services/auth-guard.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'blog-app';

  constructor(
    private authGuardService: AuthGuardService,
    private userService: UserService

  ) {

  }

  ngOnInit(){
    let user = localStorage.getItem('userId');
    if(user){
      this.authGuardService.isLoggedIn = true;
      let u = this.userService.getUser(user);

      this.authGuardService.user = u;
    }
  }

}
