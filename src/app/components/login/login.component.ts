import { Component } from '@angular/core';
import { AuthGuardService } from '../../services/auth-guard.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';
import { BlogService } from '../../services/blog.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AdminGuardService } from '../../services/admin-guard-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css',
})
export class LoginComponent {
  loginForm: FormGroup;
  constructor(
    private authGuardService: AuthGuardService,
    private router: Router,
    private userService: UserService,
    private blogService: BlogService,
    private adminGuardService: AdminGuardService
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {
    this.loginForm.reset();
  }

  login() {
    if (this.loginForm.invalid) {
      alert('Please fill all the fields');
      return;
    }

    const username = this.loginForm.get('username')?.value;
    const password = this.loginForm.get('password')?.value;
    const user = this.userService.getUser(username);

    if (!user) {
      alert('User not found');
      return;
    }

    if (user.password !== password) {
      alert('Invalid password');
      return;
    }

    this.authGuardService.isLoggedIn = true;
    this.authGuardService.user = user;
    if (user.role === 'admin') {
      this.adminGuardService.isAdmin = true;
    }


    this.router.navigate(['/blog']);
    localStorage.setItem('userId', username);
  }

  add() {
    this.blogService.addInitialBlogs();
  }
}