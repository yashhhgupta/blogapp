import { Component } from '@angular/core';
import { blog } from '../../../types';
import { BlogService } from '../../../services/blog.service';
import { Router } from '@angular/router';
import { AdminGuardService } from '../../../services/admin-guard-service';

@Component({
  selector: 'app-blog-page',
  templateUrl: './blog-page.component.html',
  styleUrl: './blog-page.component.css',
})
export class BlogPageComponent {
  blogs: blog[] = [];
  isAdmin = false;
  constructor(
    private blogService: BlogService,
    private router: Router,
    private adminGuardService: AdminGuardService
  ) {
    this.blogs = this.blogService.getBlogs();
    this.isAdmin = this.adminGuardService.isAdmin;
  }
  ngOnInit(): void {
    this.isAdmin = this.adminGuardService.isAdmin;
  }
  view(id: number) {
    this.router.navigate(['/blog', id]);
  }
  editBlog(blogId: number) {
    this.router.navigate(['/editBlog', blogId]);
  }
  deleteBlog(blogId: number) {
    this.blogService.deleteBlog(blogId);
    this.blogs = this.blogService.getBlogs();
  }
}
