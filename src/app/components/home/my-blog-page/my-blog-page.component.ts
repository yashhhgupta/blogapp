import { Component } from '@angular/core';
import { BlogService } from '../../../services/blog.service';
import { blog } from '../../../types';
import { AuthGuardService } from '../../../services/auth-guard.service';
import { UserService } from '../../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-blog-page',
  templateUrl: './my-blog-page.component.html',
  styleUrl: './my-blog-page.component.css',
})
export class MyBlogPageComponent {
  myBlogs: blog[] = [];
  user: string | null | undefined = null;
  constructor(
    private blogService: BlogService,
    private authGuardService: AuthGuardService,
    private userService: UserService,
    private router : Router
  ) {
    this.user = this.authGuardService.user?.username;
    this.myBlogs = this.blogService.getMyBlogs(this.user);
  }
  editBlog(blogId: number) {
    this.router.navigate(['/editBlog', blogId]);
  }
  deleteBlog(blogId: number) {

    this.blogService.deleteBlog(blogId);
    this.myBlogs = this.blogService.getMyBlogs(this.user);
  }
  view(blogId: number) {
    this.router.navigate(['/blog', blogId]);
  }
  // get() {
  //   this.userService.addInitialUsers();
  // }
}
