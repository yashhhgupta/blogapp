import { Component } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { user } from '../../../types';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrl: './manage-users.component.css'
})
export class ManageUsersComponent {
  users: user[] = [];
  username: string = '';
  password: string = '';
  role : "admin" | "user" = "user";

  constructor(
    private userService: UserService,
  ) {
    this.users = this.userService.getUsers();
  }
  deleteUser(username: string) {
    this.userService.deleteUser(username);
    this.users = this.userService.getUsers();
  }
  addUser() {
    if (!this.username || !this.password) { 
      alert('Please enter username and password');
      return;
    }
    //if user already exists
    if (this.users.find(user => user.username === this.username)) {
      alert('User already exists');
      return;
    }
    this.userService.addUser({
      username: this.username,
      password: this.password,
      role: this.role
    });

    this.users = this.userService.getUsers();
  }
}
