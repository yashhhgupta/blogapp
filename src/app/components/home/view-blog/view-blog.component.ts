import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { blog } from '../../../types';
import { BlogService } from '../../../services/blog.service';
@Component({
  selector: 'app-view-blog',
  templateUrl: './view-blog.component.html',
  styleUrl: './view-blog.component.css',
})
export class ViewBlogComponent {
  blogId!: number;
  blog!: blog;
  constructor(private route: ActivatedRoute,
    private blogService: BlogService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      const id = params['id']; 
      this.blogId = id;
    });
    this.blog = this.blogService.getBlog(this.blogId);
  }
}
