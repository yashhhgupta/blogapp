import { Component } from '@angular/core';
import { BlogService } from '../../../services/blog.service';
import { blog } from '../../../types';
import { Router } from '@angular/router';
import { AuthGuardService } from '../../../services/auth-guard.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup,FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrl: './create-blog.component.css',
})
export class CreateBlogComponent {
  blogForm: FormGroup;
  blogId: number | null= null;
  constructor(
    private blogService: BlogService,
    private router: Router,
    private authGuardService: AuthGuardService,
    private route: ActivatedRoute
  ) {
    this.blogForm = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      content: new FormControl('', [Validators.required, Validators.minLength(100)]),
    });
  }
  ngOnInit(): void {
    let iddd = null;
    this.route.params.subscribe((params) => {
      const id = params['id'];
      iddd = id;
    });
    let blog = this.blogService.getBlog(iddd);
    if (blog) {
      this.blogId = blog.id;
      this.blogForm.get('title')?.setValue(blog.title);
      this.blogForm.get('description')?.setValue(blog.description);
      this.blogForm.get('content')?.setValue(blog.content);
    }
  }
  onSaveBlog() {
    if (this.blogForm.invalid) { 
      return alert('Please fill all the fields');
    }

    if (this.authGuardService.user == null) {
      return alert('Please login to create a blog');
    }
    const title = this.blogForm.get('title')?.value;
    const description = this.blogForm.get('description')?.value;
    const content = this.blogForm.get('content')?.value;

    if (this.blogId) {
      let updatedBlog: blog = {
        id: this.blogId,
        title: title,
        description: description,
        content: content,
        author: this.authGuardService.user?.username,
      };
      this.blogService.updateBlog(this.blogId, updatedBlog);
      this.router.navigate(['/blog']);
      return;
    }
    
    const id = Math.floor(Math.random() * 100000);
    
    let newBlog: blog = {
      id: id,
      title: title,
      description: description,
      content: content,
      author: this.authGuardService.user.username,
    };
    this.blogService.createBlog(newBlog);
    this.router.navigate(['/blog']);
  }
}
