import { Component } from '@angular/core';
import { AuthGuardService } from '../../../services/auth-guard.service';
import { Router } from '@angular/router';
import { AdminGuardService } from '../../../services/admin-guard-service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrl: './sidebar.component.css',
})
export class SidebarComponent {
  isAdmin: boolean = false;
  selectedOption: string = '/blog';
  sidebarOptions: any = [
    { name: 'Blogs', link: '/blog' },
    { name: 'MyBlogs', link: '/myBlog' },
  ];
  constructor(
    private authGuardService: AuthGuardService,
    private router: Router,
    private adminGuardservice: AdminGuardService
  ) {
    let user = this.authGuardService.user;
    if (user) {
      this.isAdmin = user.role === 'admin';
    }
  }
  ngOnInit() {
    this.sidebarOptions = this.isAdmin
      ? [
          { name: 'Blogs', link: '/blog' },
          { name: 'Manage Users', link: '/manageUsers' },
        ]
      : [
          { name: 'Blogs', link: '/blog' },
          { name: 'MyBlogs', link: '/myBlog' },
        ];
  }

  logout() {
    this.authGuardService.isLoggedIn = false;
    this.authGuardService.user = null;
    this.adminGuardservice.isAdmin = false;
    this.router.navigate(['/login']);
    localStorage.removeItem('userId');
  }
  redirectToCreateBlog() {
    this.selectedOption = "";
    this.router.navigate(['/createBlog']);
  }
}
