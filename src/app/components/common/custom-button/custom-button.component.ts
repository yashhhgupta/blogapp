import { Input, Output, EventEmitter,Component } from '@angular/core';

@Component({
  selector: 'app-custom-button',
  templateUrl: './custom-button.component.html',
  styleUrl: './custom-button.component.css',
})
export class CustomButtonComponent {
  @Input() btnClass: string = '';
  @Input() disabled: boolean = false;
  @Output() clickEvent = new EventEmitter<void>();


  onClick(): void {
    if(this.disabled) return;
    this.clickEvent.emit();
  }
}
